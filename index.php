<?php

include 'functions.php';
date_default_timezone_set('Europe/Kiev');

$namesMonth = [
    'Vitalii',
    'Rob',
    'Bob',
    'Jonh',
];

$name = 'Bob';
$total = 100;
if ($total >= 100 && in_array($name, $namesMonth)) {
    $total *= 0.95;
}

echo $total;
echo '<br>';

if (dateChecker()){
    echo date('Y-m-d H:i:s'). '<br>';
}

echo phpversion();
echo '<br>';

//switch
$number = 10;

switchNumber($number);